# Connector SDK

essentim client library to interact with essentim sensors via mqtt backend.

## Installation

```shell
$ npm install @essentim/connector
```

## About

This is a small client library that translates sensor information form mqtt to JS-object.
To implement a new integration to other platforms, you can connect to the mqtt backend with this
library. The connector will give you methods to query devices and work
directly with the JS representations.

## Usage

### Init Client:

```js
const Connector = require("@essentim/connector")

const gateway = Connector.connect("mqtt://127.0.0.1:1883", { ... })

gateway.devices()
    .then((devices) => {
        console.log(`${device.name}: ${device.status.connection}`);
    }
})
```

### Subscribe Sensor values

```js
const device = await gateway.device('DEVICE_ID');

function printValue(sensor, datapoint) {
    console.log(`new value for ${sensor.name}: ${datapoint.value});
}

device.sensors().forEach(s => {
        s.subscibe('data', printValue)
    })
```


# Types

### Device

### Sensor

### Datapoint

### Message

