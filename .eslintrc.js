module.exports = {
    "env": {
        "node": true
    },
    "parser": "@typescript-eslint/parser",
    "plugins": ['import', '@typescript-eslint'],
    "extends": [
      "airbnb-base",
      "plugin:@typescript-eslint/recommended",
      'plugin:prettier/recommended',
      'prettier/@typescript-eslint',
    ],

    "parserOptions": {
        "ecmaVersion": 2018,
        "project": [
            "./tsconfig.json",
            "./tsconfig.example.json",
            "./tsconfig.spec.json"
        ],
        "sourceType": 'module',
    },
    "rules": {
        "comma-dangle": ["warn", "never"],
        "import/first": 0,
        "import/newline-after-import": "error",
        "import/prefer-default-export": 0,
        "no-useless-constructor": 0,
        "lines-between-class-members": 0,
        "import/order": [
            "error",
            {
                "groups": [
                    "builtin",
                    "external",
                    "internal",
                    "parent",
                    "sibling",
                    "index"
                ],
                "newlines-between": "always-and-inside-groups"
            }
        ],
        "max-lines": [
            "error",
            { "max": 300, "skipComments": true, "skipBlankLines": true }
        ],
        "newline-before-return": "warn",
        "no-unused-vars": [
            "warn",
            {
                "args": "all",
                "argsIgnorePattern": "^_",
                "vars": "all"
            }
        ],
        "padded-blocks": 0,
        "space-in-parens": 0,
        "spaced-comment": [
            "error",
            "always",
            {
                "line": {
                    "markers": ["/"],
                    "exceptions": ["-", "+"]
                },
                "block": {
                    "markers": ["!"],
                    "exceptions": ["*"],
                    "balanced": true
                }
            }
        ],
        "import/extensions": ["error", "ignorePackages", {
            "js": "never",
            "ts": "never",
            "json": "never"
        }],
    },
    "overrides": [
        {
            "files": [
                "./src/**/__tests__/**/*.ts",
                "./src/**/__mocks__/**/*.ts",
                "./src/test/**/*.ts"
            ],
            "plugins": ["jest"],
            "env": {
                "jest/globals": true
            },
            "rules": {
                "jest/no-disabled-tests": "warn",
                "jest/no-focused-tests": "error",
                "jest/no-identical-title": "error",
                "jest/prefer-to-have-length": "warn",
                "jest/valid-expect": "error",
                "@typescript-eslint/ban-ts-ignore": "warn"
            }
        },
        {
            "files": [
                "./example/**/*"
            ],
            "rules": {
                "@typescript-eslint/no-var-requires": "off",
            }
        }
    ],
    "settings": {
        "import/extensions": [".js", ".ts", ".json"],
        "import/parsers": {
            "@typescript-eslint/parser": [".ts"]
        },
        "import/resolver": {
            "node": {
                "extensions": [".js", ".ts", ".json"]
            }
        }
    },
};
