import {
  AsyncMqttClient,
  IClientSubscribeOptions,
  ISubscriptionGrant,
  IClientPublishOptions,
  IPublishPacket,
  connectAsync,
  IClientOptions
} from 'async-mqtt';
import {
  BehaviorSubject,
  Observable,
  Subject,
  MonoTypeOperatorFunction
} from 'rxjs';
import { tap } from 'rxjs/operators';
import * as mqttWildcard from 'mqtt-wildcard';

import { MqttMessage } from '../types';
import { filterEmpty } from '../utils';

export class RxMqttClient {
  private readonly topics: { [key: string]: BehaviorSubject<MqttMessage> } = {};
  private readonly messages$ = new Subject<MqttMessage>();

  constructor(public readonly client: AsyncMqttClient) {}

  subscribe(
    topic: string | string[],
    opt?: IClientSubscribeOptions
  ): Promise<ISubscriptionGrant[]> {
    return this.client.subscribe(topic, opt);
  }

  topic(topic: string): Observable<MqttMessage> {
    const subject$ = this.topics[topic];
    if (!Object.values(this.topics).length) {
      this.subscribeToMessages();
    }
    if (!subject$) {
      this.subscribe(topic);
      this.topics[topic] = new BehaviorSubject<MqttMessage>(undefined);
    }

    return this.topics[topic].pipe(filterEmpty());
  }

  pipeTo = (
    topic: string,
    opt?: IClientPublishOptions
  ): MonoTypeOperatorFunction<string | Buffer> =>
    tap((message: string | Buffer) => {
      this.publish(topic, message, opt);
    });

  async publish(
    topic: string,
    message: string | Buffer,
    opt?: IClientPublishOptions
  ): Promise<IPublishPacket> {
    return this.client.publish(topic, message, opt);
  }

  // Find all topic names (including wildcards), that match to the given one
  private matchTopics(topic): string[] {
    const matchers = Object.keys(this.topics);

    return matchers.filter(matcher => !!mqttWildcard(topic, matcher));
  }

  private subscribeToMessages(): void {
    this.client.on('message', (topic, message, opt): void => {
      const subjectMessage: MqttMessage = { topic, message, opt };
      this.messages$.next(subjectMessage);

      // Could be multiple interested subsribers because of possible wildcard ussage
      const matchedTopics = this.matchTopics(topic);

      matchedTopics.forEach(matchedTopic => {
        const subject$ = this.topics[matchedTopic];
        subject$.next(subjectMessage);
      });
    });
  }
}

export async function rxConnect(
  url: string,
  config?: IClientOptions
): Promise<RxMqttClient> {
  return new RxMqttClient(await connectAsync(url, config));
}
