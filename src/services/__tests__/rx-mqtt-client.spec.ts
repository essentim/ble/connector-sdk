import * as mqtt from 'async-mqtt';
import { Subject, from } from 'rxjs';

import { rxConnect, RxMqttClient } from '../rx-mqtt-client';

const mqttStoreStub: mqtt.Store = {
  put: jest.fn(),
  createStream: jest.fn(),
  del: jest.fn(),
  get: jest.fn(),
  close: jest.fn()
};

const mqttClientStub: mqtt.AsyncMqttClient = {
  queueQoSZero: false,
  connected: true,
  disconnected: true,
  options: {},
  outgoingStore: mqttStoreStub,
  incomingStore: mqttStoreStub,
  reconnecting: false,
  disconnecting: false,
  handleMessage: jest.fn(),
  publish: jest.fn(),
  subscribe: jest.fn(),
  unsubscribe: jest.fn(),
  end: jest.fn(),
  reconnect: jest.fn(),
  addListener: jest.fn(),
  emit: jest.fn(),
  eventNames: jest.fn(),
  getLastMessageId: jest.fn(),
  getMaxListeners: jest.fn(),
  listenerCount: jest.fn(),
  listeners: jest.fn(),
  off: jest.fn(),
  on: jest.fn(),
  once: jest.fn(),
  prependListener: jest.fn(),
  prependOnceListener: jest.fn(),
  rawListeners: jest.fn(),
  removeAllListeners: jest.fn(),
  removeListener: jest.fn(),
  removeOutgoingMessage: jest.fn(),
  setMaxListeners: jest.fn()
};

describe('<RxMqttClient>', () => {
  const topic = 'com/essentium';
  let client: RxMqttClient;

  beforeEach(() => {
    jest.clearAllMocks();
    client = new RxMqttClient(mqttClientStub);
  });

  it('Should create RxMqttClient', () => {
    expect(client).toBeDefined();
    expect((client as any).messages$).toEqual(expect.any(Subject));
    expect((client as any).topics).toEqual({});
  });

  it('Should subscribe to topic', async () => {
    const opt: mqtt.IClientSubscribeOptions = { qos: 0 };
    await client.subscribe(topic, opt);
    expect(mqttClientStub.subscribe).toHaveBeenCalledWith(topic, opt);
  });

  it('Should pipe obseravable to topic', () => {
    const messages = ['one', 'two', 'three'];
    const obs$ = from(messages);
    const opt: mqtt.IClientPublishOptions = { qos: 0 };

    obs$.pipe(client.pipeTo(topic, opt)).subscribe();

    messages.forEach(msg => {
      expect(mqttClientStub.publish).toHaveBeenCalledWith(topic, msg, opt);
      expect(mqttClientStub.publish).toHaveBeenCalledWith(topic, msg, opt);
      expect(mqttClientStub.publish).toHaveBeenCalledWith(topic, msg, opt);
    });
  });

  it('Should publish message', () => {
    const message = 'Test';
    const opt: mqtt.IClientPublishOptions = { qos: 0 };
    client.publish(topic, message, opt);
    expect(mqttClientStub.publish).toHaveBeenCalledWith(topic, message, opt);
  });

  it('Should subscribe to messages when user read from topic at the first time', () => {
    client.topic(topic);
    expect(mqttClientStub.on).toHaveBeenCalled();
    client.topic(topic);
    client.topic('second/topic');
    client.topic('third/topic');
    expect(mqttClientStub.on).toHaveBeenCalledTimes(1);
  });

  it('Should subscribe to topic and get only messages from this topic', () => {
    const message = Buffer.from('test', 'utf-8');
    const options: mqtt.Packet = { cmd: 'pubcomp' };

    let fn: mqtt.OnMessageCallback;
    (mqttClientStub.on as jest.Mock).mockImplementation((_, cb) => {
      fn = cb;
    });

    let count = 0;

    client.topic(topic).subscribe(({ message: inner }) => {
      count += 1;
      expect(inner).toEqual(message);
    });
    fn('other/topic', Buffer.from('other'), options);
    fn(topic, message, options);

    expect(count).toBe(1);
  });
});

describe('<rxConnect>', () => {
  it('Should return gateway', async () => {
    jest.spyOn(mqtt, 'connectAsync').mockImplementation(async () => null);
    const client = await rxConnect('mqtt://127.0.0.1:1883');
    expect(client).toEqual(expect.any(RxMqttClient));
  });
});
