import * as mqtt from 'async-mqtt';

import { connect } from '../connector';
import { Gateway } from '../../models/gateway';

describe('<Connector>', () => {
  it('Should return gateway', async () => {
    jest.spyOn(mqtt, 'connectAsync').mockImplementation(async () => null);
    const gateway = await connect('mqtt://127.0.0.1:1883');
    expect(gateway).toEqual(expect.any(Gateway));
  });
});
