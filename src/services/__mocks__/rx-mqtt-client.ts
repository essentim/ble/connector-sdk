import { AsyncMqttClient } from 'async-mqtt';
import { BehaviorSubject, Subject, from } from 'rxjs';

import { MqttMessage } from '../../types';
import * as utils from '../../utils';

jest.spyOn(utils, 'mapMessagesToObject').mockImplementation(() => from([{}]));

export class RxMqttClient {
  private readonly topics: { [key: string]: BehaviorSubject<MqttMessage> } = {};
  private readonly messages$ = new Subject<MqttMessage>();

  constructor(private readonly client: AsyncMqttClient) {}

  subscribe = jest.fn();

  topic = jest.fn();

  publish = jest.fn();

  private subscribeToMessages = jest.fn();
}
