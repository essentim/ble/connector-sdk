import { IClientOptions } from 'async-mqtt';

import { Gateway } from '../models';

import { ConnectorConfig } from '../types';

import { rxConnect } from './rx-mqtt-client';

export async function connect(
  url: string,
  config?: IClientOptions,
  connectorConfig?: ConnectorConfig
): Promise<Gateway> {
  return new Gateway(await rxConnect(url, config), connectorConfig);
}
