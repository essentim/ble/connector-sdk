import { IPublishPacket } from 'async-mqtt';
import { Message } from '@essentim/types';

import { subTopic } from '../utils';
import {
  Connector,
  ConnectorConfig,
  ConnectorState,
  ConnectorStateActive
} from '../types';
import { RxMqttClient } from '../services/rx-mqtt-client';

import { TopicModel } from './topic-model';

const authorizeTopicName = 'authorize';
const credentialsTopicName = 'credentials';
const stateTopicName = 'state';

export class ConnectorModel extends TopicModel<Connector> {
  constructor(
    protected readonly client: RxMqttClient,
    public readonly baseTopic: string,
    private readonly config: ConnectorConfig
  ) {
    super(client, subTopic(baseTopic));
  }

  authorize(): TopicModel<any> {
    return new TopicModel<any>(
      this.client,
      subTopic(this.baseTopic, authorizeTopicName)
    );
  }

  credentials(): TopicModel<any> {
    return new TopicModel<any>(
      this.client,
      subTopic(this.baseTopic, credentialsTopicName)
    );
  }

  state(): TopicModel<ConnectorState> {
    return new TopicModel<ConnectorState>(
      this.client,
      subTopic(this.baseTopic, stateTopicName)
    );
  }

  setCredentials(credentials: any): Promise<IPublishPacket> {
    return this.client.publish(
      subTopic(this.baseTopic, credentialsTopicName),
      JSON.stringify(credentials),
      {
        retain: true,
        qos: 0
      }
    );
  }

  async setStateMessage(message: Message): Promise<IPublishPacket> {
    const currentState = await this.state()
      .value()
      .catch(() => ({}));

    const updatedState = {
      ...currentState,
      message
    };

    return this.client.publish(
      subTopic(this.baseTopic, stateTopicName),
      JSON.stringify(updatedState),
      {
        retain: true,
        qos: 0
      }
    );
  }

  setStateDisabled(message?: Message): Promise<IPublishPacket> {
    return this.setStateTo(ConnectorStateActive.DISABLED, message);
  }

  setStateStarting(message?: Message): Promise<IPublishPacket> {
    return this.setStateTo(ConnectorStateActive.STARTING, message || null);
  }

  setStateRunning(message?: Message): Promise<IPublishPacket> {
    return this.setStateTo(ConnectorStateActive.RUNNING, message || null);
  }

  setStateDegraded(message?: Message): Promise<IPublishPacket> {
    return this.setStateTo(ConnectorStateActive.DEGRADED, message || null);
  }

  setStateStopped(message?: Message): Promise<IPublishPacket> {
    return this.setStateTo(ConnectorStateActive.STOPPED, message);
  }

  setStateFailed(message?: Message): Promise<IPublishPacket> {
    return this.setStateTo(ConnectorStateActive.FAILED, message);
  }

  private async setStateTo(
    active: ConnectorStateActive,
    message?: Message
  ): Promise<IPublishPacket> {
    const updatedState: ConnectorState = {
      active,
      message: message || undefined,
      since: new Date().toISOString(),
      name: this.config.name
    };

    return this.client.publish(
      subTopic(this.baseTopic, stateTopicName),
      JSON.stringify(updatedState),
      {
        retain: true,
        qos: 0
      }
    );
  }
}
