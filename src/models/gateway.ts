import { subTopic } from '../utils';
import { DevicePreview, ConnectorConfig, SystemInfo } from '../types';
import { RxMqttClient } from '../services/rx-mqtt-client';

import { TopicModel } from './topic-model';
import { DeviceModel } from './device-model';
import { ConnectorModel } from './connector-model';

const baseTopic = 'com/essentim';
const systemTopicName = 'system';
const gatewayTopicName = 'gateway';
const connectorsTopicName = 'connectors';

export class Gateway {
  private readonly gatewayTopic = subTopic(baseTopic, gatewayTopicName);
  private readonly infoTopic = subTopic(baseTopic, systemTopicName, 'info');

  constructor(
    private readonly client: RxMqttClient,
    private readonly connectorConfig?: ConnectorConfig
  ) {}

  info(): TopicModel<SystemInfo> {
    return new TopicModel<SystemInfo>(this.client, this.infoTopic);
  }

  connector(): ConnectorModel {
    if (!this.connectorConfig) {
      throw new Error(
        'Connector config should be provided to use ConnectorModel'
      );
    }

    const connectorTopic = subTopic(
      baseTopic,
      connectorsTopicName,
      this.connectorConfig.id
    );

    return new ConnectorModel(
      this.client,
      connectorTopic,
      this.connectorConfig
    );
  }

  devices(): TopicModel<DevicePreview[]> {
    return new TopicModel<DevicePreview[]>(
      this.client,
      subTopic(this.gatewayTopic, 'available')
    );
  }

  device(deviceId: string): DeviceModel {
    return new DeviceModel(
      this.client,
      subTopic(this.gatewayTopic, 'devices'),
      deviceId
    );
  }
}
