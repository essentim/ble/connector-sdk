import { Observable, Subscription } from 'rxjs';

import { Model } from '../types';
import { RxMqttClient } from '../services/rx-mqtt-client';
import { mapMessagesToObject, takeWithTimeoutFallback } from '../utils';

export class TopicModel<T extends Record<string, any>> implements Model<T> {
  public readonly messages$: Observable<T> = mapMessagesToObject<T>(
    this.client.topic(this.topic)
  );

  constructor(
    protected readonly client: RxMqttClient,
    public readonly topic: string
  ) {}

  async value(timeout?: number): Promise<T> {
    return this.messages$
      .pipe(
        takeWithTimeoutFallback<T>(
          timeout,
          `Can't get model value from topic <${this.topic}>.`
        )
      )
      .toPromise();
  }

  subscribe(
    next?: (value: T) => void,
    error?: (error: any) => void,
    complete?: () => void
  ): Subscription {
    return this.messages$.subscribe({ next, error, complete });
  }
}
