import { RxMqttClient } from '../../services/rx-mqtt-client';

import { Gateway } from '../gateway';
import { TopicModel } from '../topic-model';
import { DeviceModel } from '../device-model';

jest.mock('../../services/rx-mqtt-client');

const mockMqttClient = {
  publish: jest.fn(),
  subscribe: jest.fn(),
  on: jest.fn()
};

describe('<Gateway>', () => {
  let gateway: Gateway;
  let client: RxMqttClient;

  beforeEach(() => {
    // @ts-ignore
    client = new RxMqttClient(mockMqttClient);
    gateway = new Gateway(client);
  });

  it('Should create gateway', () => {
    expect(gateway).toBeDefined();
  });

  it('Should return connector topic', () => {
    const gatewayWithConnectorId = new Gateway(client, {
      id: 'id',
      name: 'name'
    });
    const topic = gatewayWithConnectorId.connector();
    expect(topic).toEqual(expect.any(TopicModel));
  });

  it('Should throw error while returning connector topic', () => {
    try {
      gateway.connector();
    } catch (err) {
      expect(gateway.connector).toThrowError();
      expect(err.message).toEqual(
        'Connector config should be provided to use ConnectorModel'
      );
    }
  });

  it('Should return devices topic', () => {
    const topic = gateway.devices();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual('com/essentim/gateway/available');
  });

  it('Should return single device topic', async () => {
    const deviceId = 'CF:12:SD:12';
    jest
      .spyOn(gateway, 'devices')
      .mockImplementation(
        () => ({ value: () => [{ address: deviceId }] } as any)
      );
    const topic = await gateway.device(deviceId);
    expect(topic).toEqual(expect.any(DeviceModel));
    expect((topic as any).topic).toEqual(
      `com/essentim/gateway/devices/${deviceId}/config`
    );
  });

  it('Should throw error when device does not exist', async () => {
    const deviceId = 'CF:12:SD:12';
    jest
      .spyOn(gateway, 'devices')
      .mockImplementation(() => ({ value: () => [] } as any));
    try {
      gateway.device(deviceId);
    } catch (err) {
      expect(err).toEqual({
        error: `Device with id=<${deviceId}> does not exist.`
      });
    }
  });
});
