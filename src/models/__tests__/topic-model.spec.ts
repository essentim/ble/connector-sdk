import { BehaviorSubject } from 'rxjs';

import { TopicModel } from '../topic-model';

import { RxMqttClient } from '../../services/rx-mqtt-client';
import * as utils from '../../utils';

jest.mock('../../services/rx-mqtt-client');

type Message = { message: string };

describe('<TopicModel>', () => {
  const topic = 'message';
  const message = 'Hello world';
  let topicModel: TopicModel<Message>;
  let sbj$: BehaviorSubject<Message>;

  beforeEach(() => {
    sbj$ = new BehaviorSubject<Message>({ message });
    jest.spyOn(utils, 'mapMessagesToObject').mockImplementation(() => sbj$);
    const client = new RxMqttClient(null);
    topicModel = new TopicModel<Message>(client, topic);
  });

  it('Should create topic', () => {
    expect(topicModel).toBeDefined();
  });

  it('Should return value', async () => {
    expect(await topicModel.value()).toEqual({ message });
  });

  it('Should subscribe to messages', () => {
    const messages: Message[] = [];
    const msgOne = 'one';
    const msgTwo = 'two';
    const msgThree = 'three';

    topicModel.messages$.subscribe((msg: Message) => messages.push(msg));
    sbj$.next({ message: msgOne });
    sbj$.next({ message: msgTwo });
    sbj$.next({ message: msgThree });

    expect(messages).toEqual([
      { message },
      { message: msgOne },
      { message: msgTwo },
      { message: msgThree }
    ]);
  });

  it('Should subscribe to model', () => {
    const messages: Message[] = [];
    const msgOne = 'one';
    const msgTwo = 'two';
    const msgThree = 'three';

    topicModel.subscribe((msg: Message) => messages.push(msg));
    sbj$.next({ message: msgOne });
    sbj$.next({ message: msgTwo });
    sbj$.next({ message: msgThree });

    expect(messages).toEqual([
      { message },
      { message: msgOne },
      { message: msgTwo },
      { message: msgThree }
    ]);
  });
});
