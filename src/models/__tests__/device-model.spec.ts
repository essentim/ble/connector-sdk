import { RxMqttClient } from '../../services/rx-mqtt-client';

import { DeviceModel } from '../device-model';
import { TopicModel } from '../topic-model';
import { SensorModel } from '../sensor-model';

jest.mock('../../services/rx-mqtt-client');
const mockMqttClient = {
  publish: jest.fn(),
  subscribe: jest.fn(),
  on: jest.fn()
};

describe('<DeviceModel>', () => {
  const deviceId = 'CD:01:AS:02';
  const baseTopic = 'base';
  let deviceModel: DeviceModel;

  beforeEach(() => {
    // @ts-ignore
    const client = new RxMqttClient(mockMqttClient);
    deviceModel = new DeviceModel(client, baseTopic, deviceId);
  });

  it('Should create model', () => {
    expect(deviceModel).toBeDefined();
    expect((deviceModel as any).topic).toEqual(
      `${baseTopic}/${deviceId}/config`
    );
  });

  it('Should return events topic', () => {
    const topic = deviceModel.events();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual(`${baseTopic}/${deviceId}/events`);
  });

  it('Should return state topic', () => {
    const topic = deviceModel.state();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual(`${baseTopic}/${deviceId}/state`);
  });

  it('Should return sensordata topic', () => {
    const topic = deviceModel.sensorsData();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual(`${baseTopic}/${deviceId}/data/+`);
  });

  it('Should return sensor model', () => {
    const sensorId = '1';
    const model = deviceModel.sensor(sensorId);
    expect(model).toEqual(expect.any(SensorModel));
  });

  it('Should return sensor models array', async () => {
    const sensors = [{ id: 1 }, { id: 2 }];
    jest
      .spyOn(deviceModel, 'value')
      .mockImplementation(() => Promise.resolve({ sensors } as any));
    const models = await deviceModel.sensors();
    expect(models).toHaveLength(2);
    expect(models).toEqual([expect.any(SensorModel), expect.any(SensorModel)]);
  });
});
