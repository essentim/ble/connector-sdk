import { BehaviorSubject } from 'rxjs';
import { Device, Sensor } from '@essentim/types';

import { RxMqttClient } from '../../services/rx-mqtt-client';
import * as utils from '../../utils';

import { SensorModel } from '../sensor-model';
import { TopicModel } from '../topic-model';

jest.mock('../../services/rx-mqtt-client');

const mockMqttClient = {
  publish: jest.fn(),
  subscribe: jest.fn(),
  on: jest.fn()
};

describe('<SensorModel>', () => {
  const deviceId = 'CD:01:AS:02';
  const baseTopic = 'base';
  const sensorId = '1';

  const sensor: Sensor = {
    id: '1',
    name: 'Temperature',
    description: 'internal',
    state: {
      connected: true,
      error: {
        message: 'generic error',
        type: 'essentim.error.namespace.and.error.type',
        data: {
          foo: 'bar'
        }
      }
    },
    quantity: {
      type: 'com.essentim.lab.quantity.temperature.celcius',
      name: 'Temperature',
      description: 'Temperature in Celcius',
      symbol: 't',
      presentation: {
        color: '#b23446',
        numeric: true,
        fractionalDigits: 1,
        icon: 'temperature',
        range: {
          min: 20,
          max: 25
        }
      }
    },
    unit: {
      type: 'org.bluetooth.unit.thermodynamic_temperature.degree_celsius',
      numeric: true,
      symbol: '°C',
      fractionalDigits: 1,
      min: 20,
      max: 25
    },
    characteristics: {
      exponent: 2,
      format: 'signed 16-bit integer'
    },
    settings: {
      disabled: false
    },
    calibration: {
      type: 'NONE',
      state: 'IDLE',
      calibrationDate: new Date().toJSON(),
      validityDate: new Date().toJSON(),
      param1: 0,
      param2: 0,
      param3: 0,
      param4: 0,
      param5: 0,
      param6: 0,
      param7: 0,
      param8: 0,
      param9: 0,
      param10: 0
    }
  };

  const device: Device = {
    id: deviceId,
    name: 'Test',
    type: 'Test type',
    info: null,
    sensors: {
      [sensorId]: sensor
    },
    actions: {}
  };

  let sensorModel: SensorModel;
  let sbj$: BehaviorSubject<Device>;

  beforeEach(() => {
    // @ts-ignore
    const client = new RxMqttClient(mockMqttClient);
    sbj$ = new BehaviorSubject<Device>(device);
    jest.spyOn(utils, 'mapMessagesToObject').mockImplementation(() => sbj$);
    sensorModel = new SensorModel(client, baseTopic, deviceId, sensorId);
  });

  it('Should create model', () => {
    expect(sensorModel).toBeDefined();
  });

  it('Should return value', async () => {
    expect(await sensorModel.value()).toEqual(sensor);
  });

  it('Should return data topic', () => {
    const topic = sensorModel.data();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual(
      `${baseTopic}/${deviceId}/data/${sensorId}`
    );
  });

  it('Should subscribe to messages', () => {
    const messages = [];

    sensorModel.messages$.subscribe(msg => messages.push(msg));
    sbj$.next(device);
    sbj$.next(device);

    expect(messages).toEqual([sensor, sensor, sensor]);
  });

  it('Should subscribe to model', () => {
    const messages = [];

    sensorModel.subscribe(msg => messages.push(msg));
    sbj$.next(device);
    sbj$.next(device);

    expect(messages).toEqual([sensor, sensor, sensor]);
  });
});
