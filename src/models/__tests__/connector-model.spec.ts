import { Severity } from '@essentim/types';

import { RxMqttClient } from '../../services/rx-mqtt-client';

import { ConnectorModel } from '../connector-model';
import { TopicModel } from '../topic-model';
import { ConnectorStateActive } from '../../types';

jest.mock('../../services/rx-mqtt-client');

const MOCK_CONNECTOR_NAME = 'mock-con-name';
const MOCK_CONNECTOR_ID = 'mock-con-id';
describe('<ConnectorModel>', () => {
  const baseTopic = 'base/connectors/default';
  const connectorConfig = {
    id: MOCK_CONNECTOR_ID,
    name: MOCK_CONNECTOR_NAME
  };
  const mockedDateISO = '2020-03-27T00:00:00.000Z';
  let client: RxMqttClient;
  let connectorModel: ConnectorModel;

  beforeEach(() => {
    (Date.prototype as any).toISOString = jest
      .fn()
      .mockReturnValueOnce(() => mockedDateISO);
    client = new RxMqttClient(null);
    connectorModel = new ConnectorModel(client, baseTopic, connectorConfig);
  });

  it('Should create model', () => {
    expect(connectorModel).toBeDefined();
    expect((connectorModel as any).topic).toEqual(baseTopic);
  });

  it('Should return authorize topic', () => {
    const topic = connectorModel.authorize();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual(`${baseTopic}/authorize`);
  });

  it('Should return credentials topic', () => {
    const topic = connectorModel.credentials();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual(`${baseTopic}/credentials`);
  });

  it('Should return state topic', () => {
    const topic = connectorModel.state();
    expect(topic).toEqual(expect.any(TopicModel));
    expect((topic as any).topic).toEqual(`${baseTopic}/state`);
  });

  it('Should call publish method of mqtt client to push new credentials ', async () => {
    const newCreds = 'creds';
    await connectorModel.setCredentials(newCreds);
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/credentials`,
      JSON.stringify(newCreds),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push update state', async () => {
    const currentState = null;
    const newStateMessage = {
      type: 'network',
      message: 'message',
      severity: Severity.INFO
    };

    connectorModel.state = jest.fn().mockReturnValueOnce({
      value: () => Promise.resolve(currentState)
    });

    await connectorModel.setStateMessage(newStateMessage);
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        ...currentState,
        message: newStateMessage
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push update state when error thrown', async () => {
    const currentState = null;
    const newStateMessage = {
      type: 'network',
      message: 'message',
      severity: Severity.INFO
    };

    connectorModel.state = jest.fn().mockReturnValueOnce({
      value: jest.fn().mockRejectedValue(null)
    });

    await connectorModel.setStateMessage(newStateMessage);
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        ...currentState,
        message: newStateMessage
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push update state merged with current one', async () => {
    const currentState = {
      active: 'running'
    };
    const newStateMessage = {
      type: 'network',
      message: 'message',
      severity: Severity.INFO
    };

    connectorModel.state = jest.fn().mockReturnValueOnce({
      value: () => Promise.resolve(currentState)
    });

    await connectorModel.setStateMessage(newStateMessage);
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        ...currentState,
        message: newStateMessage
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "DISABLED" and updated "since")', async () => {
    await connectorModel.setStateDisabled();
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.DISABLED,
        since: new Date().toISOString(),
        name: connectorConfig.name
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "DISABLED" and updated "since") when error thrown', async () => {
    await connectorModel.setStateDisabled();
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.DISABLED,
        since: new Date().toISOString(),
        name: connectorConfig.name
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "STOPPED" and updated "since")', async () => {
    await connectorModel.setStateStopped();
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.STOPPED,
        since: new Date().toISOString(),
        name: connectorConfig.name
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "STARTING", updated "since", setted state name)', async () => {
    await connectorModel.setStateStarting();
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.STARTING,
        name: connectorConfig.name,
        since: new Date().toISOString()
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "RUNNING" and updated "since")', async () => {
    await connectorModel.setStateRunning();
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.RUNNING,
        since: new Date().toISOString(),
        name: MOCK_CONNECTOR_NAME
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "DEGRADED" and updated "since")', async () => {
    await connectorModel.setStateDegraded();
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.DEGRADED,
        since: new Date().toISOString(),
        name: MOCK_CONNECTOR_NAME
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "FAILED" and updated "since")', async () => {
    await connectorModel.setStateFailed();
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.FAILED,
        since: new Date().toISOString(),
        name: MOCK_CONNECTOR_NAME
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });

  it('Should call publish method of mqtt client to push updated state (active => "FAILED" with "message" and updated "since")', async () => {
    const message = {
      type: 'Unknown',
      message: 'Runtime error',
      severity: Severity.ERROR
    };

    await connectorModel.setStateFailed(message);
    expect(client.publish).toHaveBeenCalledWith(
      `${baseTopic}/state`,
      JSON.stringify({
        active: ConnectorStateActive.FAILED,
        message,
        since: new Date().toISOString(),
        name: MOCK_CONNECTOR_NAME
      }),
      {
        qos: 0,
        retain: true
      }
    );
  });
});
