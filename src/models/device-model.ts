import { Device, DeviceState, SensorData } from '@essentim/types';
import * as RPC from 'mqtt-json-rpc';

import { subTopic } from '../utils';
import { RxMqttClient } from '../services/rx-mqtt-client';

import { TopicModel } from './topic-model';
import { SensorModel } from './sensor-model';

// do not timeout here, timeout should be handled by agent!
const REQUEST_TIMEOUT = 10 * 60 * 1000;

export class DeviceModel extends TopicModel<Device> {
  protected rpc: RPC;

  constructor(
    protected readonly client: RxMqttClient,
    public readonly baseTopic: string,
    public readonly deviceId: string
  ) {
    super(client, subTopic(baseTopic, deviceId, 'config'));
  }

  events(): TopicModel<Device> {
    return new TopicModel<Device>(
      this.client,
      subTopic(this.baseTopic, this.deviceId, 'events')
    );
  }

  state(): TopicModel<DeviceState> {
    return new TopicModel<DeviceState>(
      this.client,
      subTopic(this.baseTopic, this.deviceId, 'state')
    );
  }

  sensor(sensorId: string): SensorModel {
    return new SensorModel(
      this.client,
      this.baseTopic,
      this.deviceId,
      sensorId
    );
  }

  async sensors(): Promise<SensorModel[]> {
    const device = await this.value();

    return Object.values(device.sensors).map(
      sensor =>
        new SensorModel(this.client, this.baseTopic, this.deviceId, sensor.id)
    );
  }

  execute(
    taskName: string,
    parameters: object,
    timeout = REQUEST_TIMEOUT
  ): Promise<any> {
    // create rpc handler
    const rpc = new RPC(this.client.client, { timeout });

    return rpc.call(
      `${this.baseTopic}/${this.deviceId}/command`,
      taskName,
      ...Object.values(parameters)
    );
  }

  sensorsData(): TopicModel<SensorData> {
    return new TopicModel<SensorData>(
      this.client,
      subTopic(this.baseTopic, this.deviceId, 'data/+')
    );
  }
}
