import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { Sensor, SensorData, Device } from '@essentim/types';

import {
  subTopic,
  mapMessagesToObject,
  takeWithTimeoutFallback
} from '../utils';
import { Model } from '../types';
import { RxMqttClient } from '../services/rx-mqtt-client';

import { TopicModel } from './topic-model';

export class SensorModel implements Model<Sensor> {
  public readonly messages$: Observable<Sensor>;

  constructor(
    protected readonly client: RxMqttClient,
    public readonly baseTopic: string,
    public readonly deviceId: string,
    public readonly sensorId: string
  ) {
    const topic$ = client.topic(subTopic(this.deviceTopic, 'config'));
    this.messages$ = mapMessagesToObject<Device>(topic$).pipe(
      map(device => device.sensors[sensorId])
    );
  }

  async value(timeout?: number): Promise<Sensor> {
    return this.messages$
      .pipe(
        takeWithTimeoutFallback<Sensor>(
          timeout,
          `Can't get sensor value with id <${this.sensorId}> related to device with id <${this.deviceId}>.`
        )
      )
      .toPromise();
  }

  subscribe(
    next?: (value: Sensor) => void,
    error?: (error: any) => void,
    complete?: () => void
  ): Subscription {
    return this.messages$.subscribe({ next, error, complete });
  }

  data(): TopicModel<SensorData> {
    return new TopicModel<SensorData>(
      this.client,
      subTopic(this.deviceTopic, 'data', this.sensorId)
    );
  }

  get deviceTopic(): string {
    return subTopic(this.baseTopic, this.deviceId);
  }
}
