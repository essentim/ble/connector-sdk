interface DeviceConnection {
  type: string;
  address: string;
  rssi: number;
  connected: boolean;
}

export interface DevicePreview {
  id: string;
  name: string;
  connection: DeviceConnection;
  since: string;
}

interface ProductionInfo {
  lot_number: string;
  hardware_revision: string;
  manufacturer_ref: string;
}

export interface SystemInfo {
  serial_number: string;
  nickname: string;
  device_type: string;
  software_version: string;
  mac_address: string;
  zerotier_node: string;
  production_info: ProductionInfo;
}
