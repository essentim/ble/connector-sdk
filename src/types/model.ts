import { Observable, Subscription } from 'rxjs';

export interface Model<T> {
  readonly messages$: Observable<T>;
  value(timeout?: number): Promise<T | null>;
  subscribe(
    next?: (value: T) => void,
    error?: (error: any) => void,
    complete?: () => void
  ): Subscription;
}
