import { Message } from '@essentim/types';

export interface ConnectorConfig {
  id: string;
  name: string;
}

export const enum ConnectorStateActive {
  DISABLED = 'disabled',
  STOPPED = 'stopped',
  STARTING = 'starting',
  RUNNING = 'running',
  DEGRADED = 'degraded',
  FAILED = 'failed'
}

export interface ConnectorState {
  name: string;
  message: Message;
  active: ConnectorStateActive;
  since: string; // Should be updated when "active" changes (timestamp)
}

export interface Connector {
  state: ConnectorState;
  credentials: object;
}
