import { Packet } from 'async-mqtt';

export interface MqttMessage {
  topic: string;
  message: string | Buffer;
  opt?: Packet;
}
