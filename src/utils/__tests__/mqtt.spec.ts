import { of, from, never } from 'rxjs';

import { tap } from 'rxjs/operators';

import { MqttMessage } from '../../../lib';

import {
  subTopic,
  mapMessagesToObject,
  mapObjectToMessage,
  takeWithTimeoutFallback,
  mapMessegeToObject,
  pipeToClientTopic
} from '../mqtt';

describe('MQTT utils', () => {
  it('should generate sub topic', () => {
    expect(subTopic('com', 'essentim', 'devices')).toEqual(
      'com/essentim/devices'
    );
  });

  it('should map messages in obervable to objects', done => {
    const object = { message: 'Test' };
    const obs$ = of({
      message: Buffer.from(JSON.stringify(object), 'utf-8')
    } as MqttMessage);
    mapMessagesToObject(obs$).subscribe(parsedObject => {
      expect(parsedObject).toEqual(object);
      done();
    });
  });

  it('should map deleted messages to null', done => {
    const obs$ = of({
      message: Buffer.from([])
    } as MqttMessage);
    mapMessagesToObject(obs$).subscribe(parsedObject => {
      expect(parsedObject).toEqual(null);
      done();
    });
  });

  it('should map invalid messages to null', done => {
    const obs$ = of({} as MqttMessage);
    mapMessagesToObject(obs$).subscribe(parsedObject => {
      expect(parsedObject).toEqual(null);
      done();
    });
  });

  it('should map object to message', done => {
    const object = { message: 'Test' };
    from([object])
      .pipe(mapObjectToMessage())
      .subscribe(message => {
        expect(message).toEqual(JSON.stringify(object));
        done();
      });
  });

  it('should take one from observable', async () => {
    const testEvent = 'test';
    expect(
      await of(testEvent)
        .pipe(takeWithTimeoutFallback(0))
        .toPromise()
    ).toEqual(testEvent);
  });

  it('should try to take one from observable and gets the error', async () => {
    const errorMsg = 'Error';
    try {
      await never()
        .pipe(takeWithTimeoutFallback(1, errorMsg))
        .toPromise();
    } catch (err) {
      expect(err.message).toEqual(errorMsg);
    }
  });

  it('should map message to object', done => {
    const object = { message: 'Test' };
    const obs$ = of({
      message: Buffer.from(JSON.stringify(object), 'utf-8')
    } as MqttMessage);
    obs$.pipe(mapMessegeToObject()).subscribe(value => {
      expect(value).toEqual(object);
      done();
    });
  });

  it('should map deleted message to null', done => {
    const obs$ = of({
      message: Buffer.from([])
    } as MqttMessage);
    obs$.pipe(mapMessegeToObject()).subscribe(value => {
      expect(value).toEqual(null);
      done();
    });
  });

  it('should map invalid message to null', done => {
    const obs$ = of({} as MqttMessage);
    obs$.pipe(mapMessegeToObject()).subscribe(value => {
      expect(value).toEqual(null);
      done();
    });
  });

  it('should pipe to client topic', done => {
    const object = { message: 'Test' };
    const pipe = tap<string | Buffer>(value => {
      expect(value).toEqual(JSON.stringify(object));
    });

    of(object)
      .pipe(pipeToClientTopic(pipe))
      .subscribe(value => {
        expect(value).toEqual(object);
        done();
      });
  });
});
