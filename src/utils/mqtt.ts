import {
  Observable,
  OperatorFunction,
  MonoTypeOperatorFunction,
  pipe,
  UnaryFunction,
  throwError
} from 'rxjs';
import { map, filter, take, timeout, catchError } from 'rxjs/operators';

import { MqttMessage } from '../types';

export function subTopic(...parts: string[]): string {
  return parts.join('/');
}

export function mapMessagesToObject<T>(
  topic$: Observable<MqttMessage>
): Observable<T> {
  return topic$.pipe(
    map(value => {
      if (!value || !value.message) return null;
      if (value.message.length === 0) return null;

      return JSON.parse(value.message.toString());
    })
  );
}

export function mapMessegeToObject<T>(): OperatorFunction<MqttMessage, T> {
  return map<MqttMessage, T>(value => {
    if (!value || !value.message) return null;
    if (value.message.length === 0) return null;

    return JSON.parse(value.message.toString());
  });
}

export function mapObjectToMessage<T>(): OperatorFunction<T, string> {
  return map<T, string>(value => JSON.stringify(value));
}

export function pipeToClientTopic<T>(
  topicPipe: MonoTypeOperatorFunction<string | Buffer>
): UnaryFunction<Observable<T>, Observable<T>> {
  return pipe(
    mapObjectToMessage(),
    topicPipe,
    map(value => JSON.parse(value.toString()))
  );
}

export function filterEmpty<T>(): MonoTypeOperatorFunction<T> {
  return filter<T>(value => !!value);
}

export function takeWithTimeoutFallback<T>(
  time = 1000,
  error = `Observable does not emit any value during ${time} ms.`
): UnaryFunction<Observable<T>, Observable<T>> {
  return pipe(
    take(1),
    timeout(time),
    catchError(() => throwError(new Error(error)))
  );
}
