/* eslint-disable no-console */
const { connect } = require('../lib/index');

(async function app(deviceId, name) {
  const gateway = await connect('mqtt://192.168.200.3:1883');
  const devices = await gateway.devices().value();

  if (!name) {
    console.error('usage: node exec.js <device-id> <name>');
    process.exit(1);
  }

  if (!deviceId) {
    console.log('Known devices: ', devices.length);
    console.log('Please pass device-id as parameter:');

    await Promise.all(
      devices.map(async dev => {
        // get state for device
        const devStatus = await gateway
          .device(dev.id)
          .state()
          .value(1000);
        console.log(
          `[${devStatus.connectivity}] ${dev.id} - ${dev.name} ${
            devStatus.registration !== 'success'
              ? `(${devStatus.registration}/${devStatus.ignored})`
              : ''
          }`
        );
      })
    );
    process.exit(0);
  } else {
    console.log(`selected device with id: ${deviceId}`);
  }

  try {
    const device = await gateway.device(deviceId);
    console.log(`setting name to '${name}'...`);
    const result = await device.execute('setName', { name }, 2000);
    console.log(`result: ${result}`);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
  process.exit(0);
})(process.argv[2], process.argv[3]);
