/* eslint-disable no-console */
const { connect } = require('../lib/index');

(async function app(deviceId) {
  console.log(deviceId);
  const gateway = await connect('mqtt://192.168.200.3:1883');
  const devices = await gateway.devices().value();
  console.log('DEVICES Count=>', devices.length);

  if (!deviceId) {
    console.log('Please pass device-id as parameter:');

    await Promise.all(
      devices.map(async dev => {
        // get state for device
        const devStatus = await gateway
          .device(dev.id)
          .state()
          .value(1000);
        console.log(
          `[${devStatus.connectivity}] ${dev.id} - ${dev.name} ${
            devStatus.registration !== 'success'
              ? `(${devStatus.registration}/${devStatus.ignored})`
              : ''
          }`
        );
      })
    );
    process.exit(0);
  }
  try {
    const device = await gateway.device(deviceId);
    const sensor = device.sensor('2');

    console.log(`DEVICE with id=<${deviceId}>`, (await device.value()).name);
    console.log(
      `Sensor <2> of device with id=<${deviceId}>`,
      (await sensor.value()).name
    );
    console.log('Listening for data');

    sensor.data().subscribe(console.log);
  } catch (err) {
    console.error(err);
  }
})(process.argv[2]);
