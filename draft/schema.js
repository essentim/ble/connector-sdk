// copied from mobileapp project as a starting point...
// may be adjusted to match usecase and information given by mqtt

const MessageShape = PropTypes.shape({
  // id of the message to be translated with react-intl
  // e.g. essentim.device.connection.timeout
  type: PropTypes.string.isRequired,
  // readable default message in english
  // e.g. Could not connect for {retry} times
  message: PropTypes.string.isRequired,
  // params for the messate
  // e.g. { retry: 3 }
  data: PropTypes.shape(),
  severity: PropTypes.oneOf(['INFO', 'WARNING', 'ALERT'])
});

// number of active notifications to create indicators on overview pages, e.g. badges
const DeviceStateShape = PropTypes.shape({
  flashing: PropTypes.bool,
  error: MessageShape,
  summary: {
    info: PropTypes.number,
    warning: PropTypes.number,
    alert: PropTypes.number
  }
});

const DeviceConnectionShape = PropTypes.string.allow('online','offline','timeout');

const DeviceShape = PropTypes.shape({
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  // timestamp when the device was registered the first time
  registered: PropTypes.string.isRequired,
  state: DeviceStateShape.isRequired,
  connection: DeviceConnectionShape.isRequired,
  battery: PropTypes.shape({
    percentage: PropTypes.number,
    // remaining battery time in days
    remaining: PropTypes.number
  }),
  actions: PropTypes.object,
  // info object from mqtt
  properties: PropTypes.object,
  sensors: PropTypes.arrayOf(SensorShape).isRequired,
});
