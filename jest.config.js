const integrationProjectFileGlob = [
    '<rootDir>/test/**/*.spec.(js|ts)'
];

const testProjectFileGlob = [
    '<rootDir>/src/**/__tests__/**/*.spec.(js|ts)'
];

const lintProjectFileGlob = [
    '<rootDir>/src/**/*.(js|ts)',
    '<rootDir>/test/**/*.(js|ts)'
];

const collectCoverageFrom = [
    '<rootDir>/src/**/*.(js|ts)',
    '!<rootDir>/src/**/__*__/*.(js|ts)',
    '!<rootDir>/src/types/*.(js|ts)',
];

module.exports = {
    projects: [
        {
            displayName: 'test',
            testMatch: testProjectFileGlob,
            transform: {
                "^.+\\.ts?$": "ts-jest"
            },
        },
        {
            displayName: 'integration',
            testMatch: integrationProjectFileGlob,
            transform: {
                "^.+\\.ts?$": "ts-jest"
            },
        },
        {
            runner: 'jest-runner-eslint',
            displayName: 'lint',
            testMatch: lintProjectFileGlob
        }
    ],
    testEnvironment: 'node',
    moduleFileExtensions: ["ts", "js", "json"],
    collectCoverageFrom,
    collectCoverage: true,
    coverageThreshold: {
        global: {
            branches: 95,
            functions: 95,
            lines: 95,
            statements: 95
        }
    }
};
