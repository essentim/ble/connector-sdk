# Scouter MQTT IPC
This document contains the definition of the mqtt topics.

## Participants / Services
- essentim-device-info
- essentim-ble-agent  
- visionize-device-service

## Structure

```text
com
    /essentim
        /system
            /info                       // information to represent the device in other systems
            /status                     // status information about connectability
            /settings                   // JSON-RPC API to modify settings
                /network                // current network settings
                /cloud                  // current cloud settings
                /update                 // current firmware update settings
                /passwords              // hashed passwords for admin and service user
        /log                            // JSON-RPC API to query historic log entries
            /debug                      // topic to subscibe to current log entries
            /info                       
            /warning                    
            /error                     
            /critical  
        /gateway
            /available                  // list of all devices via the gateway
            /devices
                /{device-id}
                    /config             // current stored configuration for the device
                    /data
                        /{sensor-id}    // last datapoint received for the sensor
                    /state              // current state of the device
                    /command            // JSON-RPC API to send commands to the device
```

# Detailed description of the MQTT topics

## Information regarding the Gateway System

#### com/essentim/system/info
```json
{
    "serial_number":"ESS3JG400004",
    "type":"scouter",
    "hardware_version":"1.0.0",
    "software_version":"1.0.0",
    "mac_address":"b8:27:eb:a3:d8:0c",
    "hostname":"ESS3JG400004.local",
    "nickname":"Scouter Test 04",
}
```

#### com/essentim/system/status
```json
{
    "uptime": 123456, // uptime in seconds
    "connectivity": "internet",
    "cloud": "online",
    "last_transmission": "2020-01-28T15:56:23.125Z"
}
```

#### com/essentim/system/settings/network
```json
{
    "state": "connected",               <- network manager connection info
    "connectivity": "full",                
    "interfaces": {
        "eth0": {
            "type": "ethernet",
            "dhcp": true,
            "mac": "B8:27:EB:52:FA:38",
            "mtu": 1500,
            "ip": "192.168.0.145",
            "subnet": "255.255.255.0",
            "gateway": "192.168.0.1"
        },
        "eth1": {
            "type": "wifi",
            "dhcp": true,
            "mac": "34:C9:F0:8C:19:B4",
            "mtu": 1500,
            "ip": "10.42.0.12",
            "subnet": "255.255.255.0",
            "gateway": "10.42.0.1"
        }
    },
    "dns": {
        "servers": [
            "192.168.0.1"
        ],
        "interface": "eth0"
    }
}
```

#### com/essentim/system/settings/cloud
```json
{
    "provisioning": "success",
    "iothub": "iothub.essentim.cloud",
    "deviceid": "scouter23",

}
```

#### com/essentim/system/settings/update
```json
{
	"update_server": "http://mender.dev.essentim.cloud",
	"auto_update" : true,
	"channel" : "stable"
}
```

#### com/essentim/system/settings/passwords
```json
{
	"admin" : "$2b$12$tLvNa/KT5nOf17szkBnDeumHyJsltksbE6HDEYVUuygnVY5AISNgC",
	"service" : "$2b$12$k.zvngwDIO29TY0v3EA.C.T.45uliA6GssFTYeI63Oz2SAui2AE3G"
}
```

#### com/essentim/system
Api following `JSON-RPC 2.0` to send commands to the device
```json
# Request
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "method":  "reboot",
    "params":  null
}

# Respone
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "result":  true
}
```

## Devices accessible by the gateway

#### com/essentim/gateway/available
```json
[
    {   // active device of type sphere working properly
        "id" : "D8:BB:19:38:45:7D",
        "name" : "Sphere 457D",
        "registered": true,
        "type" : "sphere",
        "serial_number": "ESS1JG800017",
        "connection": {
            "type": "bluetooth",
            "address": "D8:BB:19:38:45:7D",
            "rssi": -79,
            "connected": false
        }
    },
    {   // active devcie of type scope working properly
        "id" : "D2:1C:F4:38:E3:13",
        "name" : "Scope E313",
        "registered": true,
        "type" : "scope",
        "serial_number": "ESS2JG200103",
        "connection": {
            "type": "bluetooth",
            "address": "D2:1C:F4:38:E3:13",
            "rssi": -79,
            "connected": false
        }
    },
    {
        // ignored device because its blacklisted
        "id" : "F5:1C:9E:A2:BB:A3",
        "name" : "Scope BBA3",
        "registered": false,
        "type" : "scope",
        "ignored" : "blacklisted",
        "connection": {
            "type": "bluetooth",
            "address": "F5:1C:9E:A2:BB:A3",
            "rssi": -79,
            "connected": false
        }
    },
    {   // ignored device because its not on whitelist
        "id" : "F5:1C:9E:A2:BB:A4",
        "name" : "Scope BBA4",
        "registered": false,
        "type" : "scope",
        "ignored" : "not_whitelisted",
        "connection": {
            "type": "bluetooth",
            "address": "F5:1C:9E:A2:BB:A4",
            "rssi": -79,
            "connected": false
        }
    },
    {   // unknown device that is not registered yet
        "id" : "D7:44:77:63:DC:A8",
        "name" : "Scitis dev",
        "registered": false,
        "type" : "unknown",
        "connection": {
            "type": "bluetooth",
            "address": "D7:44:77:63:DC:A8",
            "rssi": -84,
            "connected": false
        }
    },
    {   // device with poor signal.
        // gave up registering due to connection problems
        "id" : "D7:44:77:63:DC:A9",
        "name" : "Scitis dev2",
        "registered": false,
        "type" : "unknown",
        "ignored" : "could_not_connect",
        "connection": {
            "type": "bluetooth",
            "address": "D7:44:77:63:DC:A9",
            "rssi": -93,
            "connected": false
        }
    }
]
```


#### com/essentim/gateway/devices/{device-id}/config
```json
{
  "id": "D2:1C:F4:38:E3:13",
  "type": "sphere",
  "name": "sphere 38E313",
  "info": {
    "model": "sphere",
    "serial": "serialnumber",
    "fwversion": "5.1.9",
    "hwversion": "1.0.5",
    "manufacturer": "essentim",
    "config": 0,
    "magicTime": 300,
    "mode": 0,
    "remainingUptime": 0,
    "name": "sphere 38E313",
    "btAddress": "D2:1C:F4:38:E3:13"
  },
  "sensors": {
    "1": {
      "id": "1",
      "name": "Temperature",
      "description": "internal",
      "quantity": {
        "type": "com.essentim.lab.quantity.temperature.celcius",
        "name": "Temperature",
        "description": "Temperature in Celcius",
        "symbol": "t",
        "presentation": {
          "color": "#b23446",
          "numeric": true,
          "fractionalDigits": 1,
          "icon": "temperature",
          "range": {
            "min": 20,
            "max": 25
          }
        }
      },
      "unit": {
        "type": "org.bluetooth.unit.thermodynamic_temperature.degree_celsius",
        "numeric": true,
        "symbol": "°C",
        "fractionalDigits": 1,
        "min": 20,
        "max": 25
      },
      "characteristics": {
        "exponent": 2,
        "format": "signed 16-bit integer"
      },
      "settings": {
        "disabled": false
      },
      "calibration": {
        "type": "NONE",
        "state": "IDLE",
        "param1": 0,
        "param2": 0,
        "param3": 0,
        "param4": 0,
        "param5": 0,
        "param6": 0,
        "param7": 0,
        "param8": 0,
        "param9": 0,
        "param10": 0
      }
    },
    "2": {
      "id": "2",
      "name": "Humidity",
      "description": "internal",
      "quantity": {
        "type": "com.essentim.lab.quantity.humidity.relative_percentage",
        "name": "Humidity",
        "description": "Relative humidity",
        "symbol": "rH",
        "presentation": {
          "color": "#346677",
          "numeric": true,
          "fractionalDigits": 1,
          "icon": "humidity",
          "range": {
            "min": 0,
            "max": 100
          }
        }
      },
      "unit": {
        "type": "org.bluetooth.unit.percentage",
        "symbol": "%rH",
        "numeric": true,
        "fractionalDigits": 1,
        "min": 0,
        "max": 100
      },
      "characteristics": {
        "exponent": 2,
        "format": "unsigned 16-bit integer"
      },
      "settings": {
        "disabled": false
      },
      "calibration": {
        "type": "NONE",
        "state": "IDLE",
        "param1": 0,
        "param2": 0,
        "param3": 0,
        "param4": 0,
        "param5": 0,
        "param6": 0,
        "param7": 0,
        "param8": 0,
        "param9": 0,
        "param10": 0
      }
    }
  },
  "actions": {
    "magicTime": {
      "label": "Set Measurement Interval",
      "parameters": {
        "seconds": {
          "type": "integer",
          "state": "magicTime",
          "description": "measurement interval in seconds",
          "required": true
        }
      }
    },
    "mode": {
      "label": "Set Logging Mode",
      "parameters": {
        "seconds": {
          "type": "integer",
          "state": "mode",
          "description": "logging mode id",
          "required": true
        }
      }
    },
    "name": {
      "label": "Device Name",
      "parameters": {
        "name": {
          "type": "string",
          "state": "name",
          "description": "Name of the Device",
          "required": true
        }
      }
    },
    "identify": {
      "label": "Identify"
    },
    "turnOff": {
      "label": "Turn Off"
    },
    "storageReadout": {
      "label": "Fetch Data",
      "parameters": {
        "startTimestamp": {
          "description": "Start time of requested Timeframe",
          "type": "datetime",
          "required": true
        },
        "endTimestamp": {
          "description": "End time of requested Timeframe",
          "type": "datetime",
          "required": true
        }
      }
    },
    "storageDelete": {
      "label": "Reset Store"
    }
  }
}

```

#### com/essentim/gateway/devices/{device-id}/state
```json
{
  "connectivity": "online", // allow: online, offline, timeout, blocked
  "connected": "false", // allow: true, false
  "registration: "success", // allow: not_registered, pending, error, success
  "summary": {
    "info": 0,
    "warning": 1,
    "alert": 0,
    "error": 1
  },
  "messages": [
    {
      "type": "essentim.ble.device_error.unknown",
      "message": "Could not parse device config",
      "severity": "ERROR"
    },
    {
      "type": "essentim.ble.sensor.calibration_needed",
      "message": "Calibration required for {sensor}",
      "data": {
        "sensor": "PH"
      }
      "severity": "WARNING"
    }
  ]
}

```


#### com/essentim/gateway/devices/{device-id}/data/{sensor-id}
```json
{
  "config": 5,
  "sequence_number": 58147,
  "timestamp": "2020-02-10T13:50:52.000Z",
  "sensor": "8",
  "value": 20.43
}
```

#### com/essentim/gateway/devices/{device-id}/events
```json
{
  "config": 5,
  "sequence_number": 58147,
  "timestamp": "2020-02-10T13:50:52.000Z",
  "type": "essentim.event.device.button_press",
  "message": "Button {button} was pressed",
  "data": {
    "button": "1"
  }
}
```

#### com/essentim/gateway/devices/{device-id}/command
Api following `JSON-RPC 2.0` to send commands to the device
```json
# Request
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "method":  "identify",
    "params":  null
}

# Respone
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "result":  true
}
```

## Logging

#### com/essentim/log
// FIXME: essentim message type here... +include device id
Api following `JSON-RPC 2.0` to retrieve historic logs
```json
# Request
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "method":  "query",
    "params":  [ "2020-01-01 00:00:00.000", "2020-01-02 00:00:00.000" ]
}

# Respone
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "result":  [
        {
            "message" : "connection lost",
            "origin" : "D8:BB:19:38:45:7D",
            "severity" : "ERROR", // allow: ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
            "timestamp" : "2020-01-01 10:23:47.643"
        },
        {
            "message" : "connection established",
            "origin" : "D8:BB:19:38:45:7D",
            "severity" : "INFO", // allow: ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
            "timestamp" : "2020-01-01 10:30:00.643"
        }
    ]
}
```

#### com/essentim/log/{severity}
// FIXME: essentim message type here... +include device id
`severity = "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"`
```json
{
	"message" : "starting settings manager service",
	"origin" : "settings_manager",
	"severity" : "INFO",
	"timestamp" : "2016-12-16 14:51:24.900"
}
```


#### com/essentim/system/settings
API following `JSON-RPC 2.0` to modify system settings
```json
# Request
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "method":  "set_datetime",
    "params":  [ "2020-01-20T16:35:07.306Z" ]
}

# Respone
{
    "jsonrpc": "2.0",
    "id":      "d1acc980-0e4e-11e8-98f0-ab5030b47df4:d1db7aa0-0e4e-11e8-b1d9-5f0ab230c0d9",
    "result":  true
}
```

`command = "set_datetime", "set_hostname", "reset_factory_settings" `

#### com/eppendorf/gateway/settings/datetime
```json
{
	"datetime" : "2016-08-30T16:35:07.306Z"
}
```

#### com/eppendorf/gateway/settings/general
```json
{
	"device_name" : "Gateway Raum 1.2.019"
}
```

#### com/eppendorf/gateway/settings/network
```json
{
	"dhcp_ip" : false,
	"ip_address" : "192.168.20.106",
	"subnet_mask" : "255.255.255.0",
	"default_gateway" : "192.168.20.1",
	"dhcp_dns" : false,
	"primary_dns" : "8.8.8.8",
	"secondary_dns" : "8.8.8.8",
	"dhcp_ntp" : false,
	"ntp_server" : "pool.ntp.org"
}
```

#### com/eppendorf/gateway/settings/passwords
```json
{
	"user" : "$2b$12$tLvNa/KT5nOf17szkBnDeumHyJsltksbE6HDEYVUuygnVY5AISNgC",
	"service" : "$2b$12$k.zvngwDIO29TY0v3EA.C.T.45uliA6GssFTYeI63Oz2SAui2AE3G",
	"develop" : "$2b$12$tl//ejxbmEZcOGY/PJfsfuEq6XuCBeLL323VHwXShveqXGMQlzH2."
}
```

#### com/eppendorf/gateway/settings/update
```json
{
	"auto_update" : true,
	"channel" : "stable"
}
```

#### com/eppendorf/gateway/system/cmd
```json
{
	"command" : "reboot"
}
```

`command = "reboot"`

#### com/eppendorf/gateway/system/info
```json
[
	{
		"serial_number" : "1006AAX00001"
	},
	{
		"software_version" : "1.2.3.0"
	},
	{
		"mac_address" : "00:13:3f:00:00:00"
	}
]
```

#### com/eppendorf/gateway/system/status
```json
{
	"status" : "device_updated"
}

```

`status = "device_connected" | "device_disconnected" | "device_updated" | "usb_stick_attached" | "usb_stick_detached" | "software_update_found" | "software_update_in_progress" | "led_test" | "error" | "critical"`

#### com/eppendorf/gateway/update/available
```json
{
	"source" : "USB",
	"version" : "1.2.4.0"
}
```

#### com/eppendorf/gateway/update/channels
```json
[
	"develop",
	"test",
	"stable"
]
```

#### com/eppendorf/gateway/update/cmd
```json
{
	"command" : "start_update"
}
```

`command = "start_update" | "refresh"`

#### com/eppendorf/gateway/update/status
```json
{
	"message" : "settings.update.status.extracting",
	"progress" : 33.33
}
```
